package com.safetynet.alerts.services.personservice;

import com.safetynet.alerts.AlertsApplication;
import com.safetynet.alerts.models.Allergie;
import com.safetynet.alerts.models.MedicalRecordModel;
import com.safetynet.alerts.models.Medication;
import com.safetynet.alerts.models.PersonModel;
import com.safetynet.alerts.repository.medicalrecordrepository.IAllergieRepository;
import com.safetynet.alerts.repository.medicalrecordrepository.IMedicalRecordRepository;
import com.safetynet.alerts.repository.medicalrecordrepository.IMedicationRepository;
import com.safetynet.alerts.repository.personrepository.IPersonRepository;
import com.safetynet.alerts.services.medicalrecordservice.MedicalRecordService;
import org.assertj.core.api.AbstractBigDecimalAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class PersonServiceTest {

    @Mock
    private IPersonRepository personRepository;
    private PersonService personService;
    private IMedicalRecordRepository medicalRecordRepository;
    private MedicalRecordService medicalRecordService;
    private IAllergieRepository allergieRepository;
    private IMedicationRepository medicationRepository;

    @BeforeEach
    void setUp() {
        personService = new PersonService(personRepository);
        medicalRecordService = new MedicalRecordService(medicalRecordRepository, allergieRepository, medicationRepository, personRepository);

    }

    @Test
    void remove() {
        PersonModel personModel = new PersonModel();
        personModel.setFirstName("test");
        personModel.setLastName("tt");
        personModel.setId(1L);

        when(personRepository.findByFirstNameAndLastName(eq("test"), eq("tt"))).thenReturn(personModel);
        personService.remove(personModel);
        Mockito.verify(personRepository).deleteById(eq(1L));
    }

    @Test
    void create(){
        PersonModel personModel = new PersonModel();
        MedicalRecordModel medicalRecordModel = new MedicalRecordModel();
        LocalDate birthDate = LocalDate.of(1993, 1, 8);
        Medication medication = new Medication();
        Allergie allergie = new Allergie();

        medication.setName("MedicationTest");
        allergie.setName("AllergieName");

        List<Medication> medications = new ArrayList<>(){
            {
                add(medication);
            }
        };
        List<Allergie> allergies = new ArrayList<>(){
            {
                add(allergie);
            }
        };
        personModel.setFirstName("person");
        personModel.setLastName("personGet");
        personModel.setId(3L);
        personModel.setAddress("address");
        personModel.setCity("CityTest");
        personModel.setPhone("098372689");
        personModel.setEmail("test@testing.te");

        medicalRecordModel.setPersonModel(personModel);
        medicalRecordModel.setBirthdate(birthDate);
        medicalRecordModel.setMedications(medications);
        medicalRecordModel.setAllergies(allergies);

        personModel.setMedicalRecordModel(medicalRecordModel);


    }
}