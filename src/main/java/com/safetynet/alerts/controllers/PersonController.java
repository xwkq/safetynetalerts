package com.safetynet.alerts.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.safetynet.alerts.models.ContactsModel;
import com.safetynet.alerts.models.PersonModel;
import com.safetynet.alerts.services.personservice.PersonService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonController {

    private final PersonService personService;

    public PersonController(PersonService personService){
        this.personService = personService;
    }

    @GetMapping
    @ResponseBody
    public Iterable<PersonModel> getPersons() {
        return this.personService.findAll();
    }

    @PostMapping("/create")
    @ResponseBody
    public PersonModel create (@RequestBody PersonModel personModel){
        return this.personService.create(personModel);
    }

    @DeleteMapping("/remove")
    @ResponseBody
    public void remove (@RequestBody PersonModel personModel){
        this.personService.remove(personModel);
    }

    @PutMapping("/update")
    @ResponseBody
    public PersonModel update (@RequestBody PersonModel personModel) throws JsonProcessingException {
        return this.personService.update(personModel);
    }

    @GetMapping("/childAlert")
    @ResponseBody
    public Iterable<PersonModel> getChild(@RequestParam(name = "address") String address) {
        return this.personService.findChild(address);
    }

    @GetMapping("/personInfo")
    @ResponseBody
    public List<PersonModel> getPersonInfo(@RequestParam(name = "firstName") String firstName, @RequestParam(name = "lastName") String lastName) {
        return this.personService.getPersonInfo(firstName, lastName);
    }

    @GetMapping("/communityEmail")
    @ResponseBody
    public List<ContactsModel> getEmails(@RequestParam(name = "city") String city) {
        return this.personService.getEmailsByCity(city);
    }

    @GetMapping("/phoneAlert")
    @ResponseBody
    public List<ContactsModel> getPhoneNumber(@RequestParam(name = "firestation") String firestation){
        return this.personService.getPhoneByStation(firestation);
    }
}
