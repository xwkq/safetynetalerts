package com.safetynet.alerts.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.safetynet.alerts.models.FireStationModel;
import com.safetynet.alerts.models.PersonModel;
import com.safetynet.alerts.services.firestationservice.FireStationService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/firestations")
public class FireStationController {
    private final FireStationService fireStationService;

    public FireStationController(FireStationService fireStationService){
        this.fireStationService = fireStationService;
    }

    @GetMapping
    @ResponseBody
    public Iterable<FireStationModel> getFireStations() {

        return fireStationService.findAll();
    }

    @PostMapping("/update")
    @ResponseBody
    public FireStationModel update (@RequestBody FireStationModel fireStationModel) throws JsonProcessingException {
        return fireStationService.update(fireStationModel);
    }

    @GetMapping("/station")
    @ResponseBody
    public List<PersonModel> getPersonsForFireStation(@RequestParam(name = "station") String station){
        return this.fireStationService.getPersonForStation(station);
    }

    @GetMapping("/fire")
    @ResponseBody
    public List<PersonModel> getPersonByFireAddress(@RequestParam(name = "address") String address){
        return this.fireStationService.findPersonByStationAddress(address);
    }

    @GetMapping("/flood")
    @ResponseBody
    public List<PersonModel> getPersonByStations(@RequestParam(name = "stations") List<String> stations){
        return this.fireStationService.getPersonByStations(stations);
    }
}
