package com.safetynet.alerts.controllers;

import com.safetynet.alerts.models.MedicalRecordModel;
import com.safetynet.alerts.services.medicalrecordservice.MedicalRecordService;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/medicalrecord")
public class MedicalRecordController{

    private final MedicalRecordService medicalRecordService;

    public MedicalRecordController(MedicalRecordService medicalRecordService){
        this.medicalRecordService = medicalRecordService;
    }

    @GetMapping
    @ResponseBody
    public Iterable<MedicalRecordModel> getMedicalRecord() {
        return this.medicalRecordService.findAll();
    }

    @PostMapping
    @ResponseBody
    public MedicalRecordModel create (@RequestBody MedicalRecordModel medicalRecordModel){
        return  this.medicalRecordService.create(medicalRecordModel);
    }

    @DeleteMapping
    @ResponseBody
    public void remove (@RequestBody MedicalRecordModel medicalRecordModel){
        this.medicalRecordService.remove(medicalRecordModel);
    }

    @PutMapping
    @ResponseBody
    public void update (@RequestBody MedicalRecordModel medicalRecordModel){
        this.medicalRecordService.update(medicalRecordModel);
    }
}
