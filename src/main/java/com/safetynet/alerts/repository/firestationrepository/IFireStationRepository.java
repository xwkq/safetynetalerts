package com.safetynet.alerts.repository.firestationrepository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.safetynet.alerts.models.FireStationModel;
import com.safetynet.alerts.models.PersonModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IFireStationRepository extends CrudRepository<FireStationModel, Long>
{
    List<FireStationModel> findByStation(String station);
    List<FireStationModel> findByAddress(String address);

    @Query("SELECT p FROM PersonModel p WHERE p.fireStationModel.station = :station")
    List<PersonModel> findByFireStationModel(@Param("station") String station);

    @Query("SELECT p FROM PersonModel p WHERE p.fireStationModel.address = :address")
    List<PersonModel> findPersonByFireStationAddress(@Param("address") String address);
}
