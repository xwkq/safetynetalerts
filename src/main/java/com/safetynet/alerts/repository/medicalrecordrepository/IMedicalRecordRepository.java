package com.safetynet.alerts.repository.medicalrecordrepository;

import com.safetynet.alerts.models.MedicalRecordModel;
import com.safetynet.alerts.models.PersonModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface IMedicalRecordRepository extends CrudRepository<MedicalRecordModel, Long> {
}
