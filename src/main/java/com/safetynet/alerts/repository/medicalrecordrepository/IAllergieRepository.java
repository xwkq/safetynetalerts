package com.safetynet.alerts.repository.medicalrecordrepository;

import com.safetynet.alerts.models.Allergie;
import org.springframework.data.repository.CrudRepository;

public interface IAllergieRepository extends CrudRepository<Allergie, Long>{
    Allergie getByName(String name);
}
