package com.safetynet.alerts.repository.medicalrecordrepository;

import com.safetynet.alerts.models.Medication;
import org.springframework.data.repository.CrudRepository;

public interface IMedicationRepository extends CrudRepository<Medication, Long> {
    Medication getByName(String name);
}
