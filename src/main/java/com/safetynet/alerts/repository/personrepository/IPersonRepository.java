package com.safetynet.alerts.repository.personrepository;

import com.safetynet.alerts.models.ContactsModel;
import com.safetynet.alerts.models.FireStationModel;
import com.safetynet.alerts.models.PersonModel;
import org.apache.tomcat.jni.Local;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface IPersonRepository extends CrudRepository<PersonModel, Long> {
    PersonModel findByFirstNameAndLastName(String firstName, String lastName);
    List<PersonModel> findByLastName(String lastName);
    List<PersonModel> findByAddress(String address);

    @Query("SELECT p FROM PersonModel p WHERE p.fireStationModel.station = :station")
    List<PersonModel> findByFireStationModel(@Param("station") String station);

    @Query("SELECT p FROM PersonModel p WHERE p.fireStationModel.address = :address")
    List<PersonModel> findPersonByFireStationAddress(@Param("address") String address);

    @Query("SELECT p FROM PersonModel p WHERE p.medicalRecordModel.birthdate > :date  AND p.address = :address")
    List<PersonModel> findChild(@Param("address") String address, @Param("date")LocalDate date);

    @Query("SELECT p FROM PersonModel p WHERE p.city = :city")
    List<PersonModel> findEmailByCity(@Param("city") String city);

    @Query("SELECT p.phone FROM PersonModel p WHERE p.fireStationModel.station = :station")
    List<String> findPhoneByStation(@Param("station") String station);
}
