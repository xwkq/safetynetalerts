package com.safetynet.alerts.models;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity
public class MedicalRecordModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;

    private LocalDate birthdate;


    @OneToOne
    private transient PersonModel personModel;
    @OneToMany
    private List<Allergie> allergies;

    @OneToMany
    private List<Medication> medications;


    public MedicalRecordModel(){
    }

    public MedicalRecordModel(PersonModel personModel, LocalDate birthDate, List<Allergie> allergie, List<Medication> medication){
        this.personModel = personModel;
        this.birthdate = birthDate;
        this.allergies = allergie;
        this.medications = medication;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public Long getId() {
        return id;
    }

    public List<Medication> getMedications() {
        return medications;
    }

    public void setMedications(List<Medication> medications) {
        this.medications = medications;
    }

    public List<Allergie> getAllergies() {
        return allergies;
    }

    public void setAllergies(List<Allergie> allergies) {
        this.allergies = allergies;
    }

    public PersonModel getPersonModel() {
        return personModel;
    }

    public void setPersonModel(PersonModel personModel) {
        this.personModel = personModel;
    }
}
