package com.safetynet.alerts.models;

import java.util.Date;
import java.util.List;

public class MedicalRecordsJson {
    public List<String> allergies;
    public List<String> medications;
    public String firstName;
    public String lastName;
    public String birthdate;

    public MedicalRecordsJson(){
    }
}
