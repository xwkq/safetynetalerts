package com.safetynet.alerts.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class PersonModel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;

    private String city;
    private String zip;
    private String phone;
    private String email;
    private String firstName;
    private String lastName;

    @ManyToOne
    private FireStationModel fireStationModel;
    private String address;
    @OneToOne
    private MedicalRecordModel medicalRecordModel;

    public PersonModel(){}

    public PersonModel(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public PersonModel(FireStationModel fireStationModel, MedicalRecordModel medicalRecordModel, Long id, String firstName, String lastName, String address, String city, String zip, String phone, String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.zip = zip;
        this.phone = phone;
        this.email = email;
        this.medicalRecordModel = medicalRecordModel;
        this.fireStationModel = fireStationModel;
    }

    public String getFirstName() {
        return this.firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getAddress() {
       return address;
    }
    public void setAddress(String address) {
       this.address = address;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getZip() {
        return zip;
    }
    public void setZip(String zip) {
        this.zip = zip;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public Long getId() {
        return this.id;
    }

    public FireStationModel getFireStationModel() {
        return fireStationModel;
    }

    public void setFireStationModel(FireStationModel fireStationModel) {
        this.fireStationModel = fireStationModel;
    }

    public MedicalRecordModel getMedicalRecordModel() {
        return medicalRecordModel;
    }

    public void setMedicalRecordModel(MedicalRecordModel medicalRecordModel) {
        this.medicalRecordModel = medicalRecordModel;
    }
    public void setId(Long id) {
        this.id = id;
    }
}
