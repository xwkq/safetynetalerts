package com.safetynet.alerts.models;

import java.util.List;

public class InputFileModel {
    public List<PersonModel> persons;
    public List<MedicalRecordsJson> medicalrecords;
    public List<FireStationModel> firestations;
}
