package com.safetynet.alerts.models;

import java.util.List;

public class ContactsModel {
    private String email;
    private String phoneNumber;

    public ContactsModel(){

    }
    public String getEmail(){
        return this.email;
    }
    public void setEmail(String email){
        this.email = email;
    }
    public String getPhoneNumber(){
        return this.phoneNumber;
    }
    public void setPhoneNumber(String number){
        this.phoneNumber = number;
    }
}
