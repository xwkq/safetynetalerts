package com.safetynet.alerts.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Medication {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;
    @ManyToOne
    @JsonIgnore
    private MedicalRecordModel medicalRecordModel;
    private String name;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Medication(String name, MedicalRecordModel medicalRecordModel){
        this.setName(name);
        this.medicalRecordModel = medicalRecordModel;
    }

    public Medication(){};

    public void setId(Long id) {
        this.id = id;
    }

    public MedicalRecordModel getMedicalRecordModel() {
        return medicalRecordModel;
    }

    public void setMedicalRecordModel(MedicalRecordModel medicalRecordModel) {
        this.medicalRecordModel = medicalRecordModel;
    }
}
