package com.safetynet.alerts.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class FireStationModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;

    private String address;
    private String station;

    @OneToMany
    List<PersonModel> personModels;


    public FireStationModel() {}
    public FireStationModel(long id, String address, String station) {
        this.id = id;
        this.address = address;
        this.station = station;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getId() {
        return id;
    }
    public List<PersonModel> getPersonModel() {
        return personModels;
    }
    public void setPersonModel(List<PersonModel> personModel) {
        this.personModels = personModel;
    }

}
