package com.safetynet.alerts.batch;

import com.safetynet.alerts.models.*;
import com.safetynet.alerts.repository.firestationrepository.IFireStationRepository;
import com.safetynet.alerts.repository.medicalrecordrepository.IAllergieRepository;
import com.safetynet.alerts.repository.medicalrecordrepository.IMedicalRecordRepository;
import com.safetynet.alerts.repository.medicalrecordrepository.IMedicationRepository;
import com.safetynet.alerts.repository.personrepository.IPersonRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Component
public class LoadInputFileIntoDB implements ApplicationRunner {

    private final IPersonRepository personRepository;
    private final IMedicalRecordRepository medicalRecordRepository;
    private final IFireStationRepository fireStationRepository;
    private final InputFileModel inputFileModel;
    private final IMedicationRepository medicationRepository;
    private final IAllergieRepository allergieRepository;

    

    public LoadInputFileIntoDB(InputFileModel inputFileModel, IPersonRepository personRepository, IMedicalRecordRepository medicalRecordRepository, IFireStationRepository fireStationRepository, IMedicationRepository medication, IAllergieRepository allergie){
        this.inputFileModel = inputFileModel;
        this.personRepository = personRepository;
        this.medicalRecordRepository = medicalRecordRepository;
        this.fireStationRepository = fireStationRepository;
        this.medicationRepository = medication;
        this.allergieRepository = allergie;
    }
    @Override
    @Transactional
    public void run(ApplicationArguments args) throws Exception {
        this.personRepository.saveAll(this.inputFileModel.persons);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

        for(FireStationModel fireStationModel : this.inputFileModel.firestations){
            this.fireStationRepository.save(fireStationModel);
            for(PersonModel personModel : this.personRepository.findByAddress(fireStationModel.getAddress())){
                personModel.setFireStationModel(fireStationModel);
                this.personRepository.save(personModel);
            }
        }

        for(MedicalRecordsJson medicalRecordsJson : this.inputFileModel.medicalrecords){
            PersonModel personModel = this.personRepository.findByFirstNameAndLastName(medicalRecordsJson.firstName, medicalRecordsJson.lastName);

            List<Allergie> allergies = new ArrayList<>();
            List<Medication> medications = new ArrayList<>();

            MedicalRecordModel medicalRecordModel = new MedicalRecordModel(personModel, LocalDate.parse(medicalRecordsJson.birthdate, dateTimeFormatter), allergies, medications);
            this.medicalRecordRepository.save(medicalRecordModel);

            for(String allergie : medicalRecordsJson.allergies){
                Allergie al = new Allergie(allergie, medicalRecordModel);
                allergies.add(al);
                this.allergieRepository.save(al);
            }

            for(String medication : medicalRecordsJson.medications){
                Medication med = new Medication(medication, medicalRecordModel);
                medications.add(med);
                this.medicationRepository.save(med);
            }

            medicalRecordModel.setAllergies(allergies);
            medicalRecordModel.setMedications(medications);
            this.medicalRecordRepository.save(medicalRecordModel);
            personModel.setMedicalRecordModel(medicalRecordModel);
            this.personRepository.save(personModel);


        }

    }
}
