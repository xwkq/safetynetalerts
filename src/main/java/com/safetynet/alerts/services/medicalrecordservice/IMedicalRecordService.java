package com.safetynet.alerts.services.medicalrecordservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.safetynet.alerts.models.MedicalRecordModel;

public interface IMedicalRecordService {
    Iterable<MedicalRecordModel> findAll() throws JsonProcessingException;
    MedicalRecordModel create(MedicalRecordModel medicalRecordModel);
    void remove (MedicalRecordModel medicalRecordModel);
    void update(MedicalRecordModel medicalRecordModel);


}
