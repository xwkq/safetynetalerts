package com.safetynet.alerts.services.medicalrecordservice;

import com.safetynet.alerts.models.MedicalRecordModel;
import com.safetynet.alerts.models.PersonModel;
import com.safetynet.alerts.repository.medicalrecordrepository.IAllergieRepository;
import com.safetynet.alerts.repository.medicalrecordrepository.IMedicalRecordRepository;
import com.safetynet.alerts.repository.medicalrecordrepository.IMedicationRepository;
import com.safetynet.alerts.repository.personrepository.IPersonRepository;
import org.springframework.stereotype.Service;

@Service
public class MedicalRecordService implements IMedicalRecordService {
    private final IMedicalRecordRepository medicalRecordRepository;
    private final IAllergieRepository allergieRepository;
    private final IMedicationRepository medicationRepository;
    private final IPersonRepository personRepository;

    public MedicalRecordService(IMedicalRecordRepository medicalRecordRepository, IAllergieRepository allergieRepository, IMedicationRepository medicationRepository, IPersonRepository personRepository){
        this.medicalRecordRepository = medicalRecordRepository;
        this.allergieRepository = allergieRepository;
        this.medicationRepository = medicationRepository;
        this.personRepository = personRepository;
    }

    @Override
    public Iterable<MedicalRecordModel> findAll() {
        return medicalRecordRepository.findAll();
    }

    @Override
    public MedicalRecordModel create(MedicalRecordModel medicalRecordModel) {

        return this.medicalRecordRepository.save(medicalRecordModel);
    }

    @Override
    public void remove(MedicalRecordModel medicalRecordModel){
        PersonModel personModel = medicalRecordModel.getPersonModel();
        PersonModel tmpPerson = this.personRepository.findByFirstNameAndLastName(personModel.getFirstName(), personModel.getLastName());
        this.medicalRecordRepository.deleteById(tmpPerson.getMedicalRecordModel().getId());
    }

    @Override
    public void update(MedicalRecordModel medicalRecordModel){
        PersonModel personModel = medicalRecordModel.getPersonModel();
        PersonModel tmpPerson = this.personRepository.findByFirstNameAndLastName(personModel.getFirstName(), personModel.getLastName());
        tmpPerson.getMedicalRecordModel().setAllergies(medicalRecordModel.getAllergies());
        tmpPerson.getMedicalRecordModel().setMedications(medicalRecordModel.getMedications());

        this.medicalRecordRepository.save(tmpPerson.getMedicalRecordModel());
    }

}
