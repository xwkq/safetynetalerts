package com.safetynet.alerts.services.firestationservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.safetynet.alerts.models.FireStationModel;
import com.safetynet.alerts.models.PersonModel;

import java.util.List;

public interface IFireStationService {
    Iterable<FireStationModel> findAll();
    FireStationModel update(FireStationModel fireStationModel);
    Iterable<FireStationModel> findByStation(String station);

}
