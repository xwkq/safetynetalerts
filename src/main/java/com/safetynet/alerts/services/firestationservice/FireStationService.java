package com.safetynet.alerts.services.firestationservice;

import com.safetynet.alerts.models.FireStationModel;
import com.safetynet.alerts.models.PersonModel;
import com.safetynet.alerts.repository.firestationrepository.IFireStationRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FireStationService implements IFireStationService{
    private final IFireStationRepository fireStationRepository;

    public FireStationService(IFireStationRepository fireStationRepository){
        this.fireStationRepository = fireStationRepository;
    }

    @Override
    public Iterable<FireStationModel> findAll() {
        return fireStationRepository.findAll();
    }

    @Override
    public FireStationModel update(FireStationModel fireStationModel){
        return fireStationRepository.save(fireStationModel);
    }

    public Iterable<FireStationModel> findByStation(String station){
        return fireStationRepository.findByStation(station);
    }

    public List<FireStationModel> findByAddress(String address){
        return fireStationRepository.findByAddress(address);
    }

    public List<PersonModel> getPersonForStation(String station){
        return this.fireStationRepository.findByFireStationModel(station);
    }

    public List<PersonModel> getPersonByStations(List<String> stations){
        List<PersonModel> persons = new ArrayList<>();
        for(String station : stations){
            for(FireStationModel fireStationModel : findByStation(station)) {
                persons.addAll(this.fireStationRepository.findPersonByFireStationAddress(fireStationModel.getAddress()));
            }
        }
        return persons;
    }

    public List<PersonModel> findPersonByStationAddress(String address){
        return this.fireStationRepository.findPersonByFireStationAddress(address);
    }
}
