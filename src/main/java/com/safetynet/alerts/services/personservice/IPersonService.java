package com.safetynet.alerts.services.personservice;

import com.safetynet.alerts.models.PersonModel;

public interface IPersonService {
    Iterable<PersonModel> findAll();
    PersonModel create(PersonModel personModel);
    void remove(PersonModel personModel);
    PersonModel update(PersonModel personModel);
}
