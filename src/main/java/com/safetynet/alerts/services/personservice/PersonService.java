package com.safetynet.alerts.services.personservice;

import com.safetynet.alerts.models.ContactsModel;
import com.safetynet.alerts.models.PersonModel;
import com.safetynet.alerts.repository.personrepository.IPersonRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class PersonService implements IPersonService {
    private final IPersonRepository personRepository;

    public PersonService(IPersonRepository personRepository){
        this.personRepository = personRepository;
    }

    @Override
    public Iterable<PersonModel> findAll() {
        return this.personRepository.findAll();
    }

    @Override
    public PersonModel create(PersonModel personModel) {

        return this.personRepository.save(personModel);
    }


    @Override
    public void remove(PersonModel personModel) {

        PersonModel tmpPerson = this.personRepository.findByFirstNameAndLastName(personModel.getFirstName(), personModel.getLastName());
        Long idPerson = tmpPerson.getId();
        this.personRepository.deleteById(idPerson);

    }

    @Override
    public PersonModel update(PersonModel personModel) {

       PersonModel person = this.personRepository.findByFirstNameAndLastName(personModel.getFirstName(), personModel.getLastName());
            person.setFireStationModel(personModel.getFireStationModel());
            person.setAddress(personModel.getAddress());
            person.setCity(personModel.getCity());
            person.setZip(personModel.getZip());
            person.setPhone(personModel.getPhone());
            person.setEmail(personModel.getEmail());
            person.setMedicalRecordModel(personModel.getMedicalRecordModel());
            this.personRepository.save(person);

        return personModel;
    }


    public List<PersonModel> findByFireStationModel(String station) {
        return this.personRepository.findByFireStationModel(station);
    }

    public List<PersonModel> findPersonByFireStationAddress(String address) {
        return this.personRepository.findPersonByFireStationAddress(address);
    }

    public List<PersonModel> findChild(String address){
        return this.personRepository.findChild(address, LocalDate.now().minusYears(19));
    }

    public List<PersonModel> findPersonByFireAddress(String address){
        List<PersonModel> childrens = new ArrayList<>();
        List<PersonModel> others = new ArrayList<>();

        for(PersonModel personModel : this.personRepository.findPersonByFireStationAddress(address)){
            LocalDate birthDay = personModel.getMedicalRecordModel().getBirthdate();
            int years = Period.between(birthDay, LocalDate.now()).getYears();
            if(years <= 18){
                childrens.add(personModel);
                List<PersonModel> tmpPersonModel = this.personRepository.findByLastName(personModel.getLastName());
                for(PersonModel other : tmpPersonModel){
                    if(!Objects.equals(personModel.getFirstName(), other.getFirstName()))
                    others.add(other);
                }
            }
        }
        childrens.addAll(others);
        return childrens;
    }
    public List<PersonModel> getPersonInfo(String firstName, String lastName){
        List<PersonModel> personsInfo = new ArrayList<>();
        for(PersonModel personModel : getPersonSameLastname(lastName)){
            personsInfo.add(personModel);
        }
        return personsInfo;
    }
    public List<PersonModel> getPersonSameLastname(String lastName){
        return this.personRepository.findByLastName(lastName);
    }

    public List<ContactsModel> getEmailsByCity(String city){
        List<ContactsModel> emails = new ArrayList<>();
        ContactsModel emailsModel = new ContactsModel();
        for(PersonModel personModel : this.personRepository.findEmailByCity(city)){
            emailsModel.setEmail(personModel.getEmail());
            emails.add(emailsModel);
        }
        return emails;
    }

    public List<ContactsModel> getPhoneByStation(String station){

        List<ContactsModel> phonesNumber = new ArrayList<>();
        ContactsModel phoneModel = new ContactsModel();

        for(String number : this.personRepository.findPhoneByStation(station)){
            phoneModel.setPhoneNumber(number);
            phonesNumber.add(phoneModel);
        }
        return phonesNumber;
    }
}
