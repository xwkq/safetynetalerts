package com.safetynet.alerts.configurations;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.safetynet.alerts.models.InputFileModel;
import com.safetynet.alerts.utils.DateDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

@Configuration
@PropertySource("classpath:application.properties")
public class ApplicationConfiguration {

    @Value("${jsonPath}")
    public String path;

    @Bean
    public InputFileModel inputFileModel(Gson gson) {
        try {
            Reader reader = Files.newBufferedReader(Paths.get(path));
            InputFileModel inputFileModel = gson.fromJson(reader, InputFileModel.class);
            reader.close();
            return inputFileModel;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Bean
    public Gson gson(){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
        return  gsonBuilder.create();
    }

}
